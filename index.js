const fs = require('fs');
console.log("\nСинхронное чтение: ")
fs.readFileSync('./1.txt')
console.log("Прочел 1.txt");
fs.readFileSync('./2.txt')
console.log("Прочел 2.txt");

console.log("\nАссинхронное чтение: ")
fs.readFile('./1.txt', () => {console.log("Прочел 1.txt\n")})
fs.readFile('./2.txt', () => {console.log("Прочел 2.txt")})